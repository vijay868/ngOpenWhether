import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule} from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/navigation/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserService } from './services/user.service';
import { UserResolver } from './resolvers/user.resolver';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { TodoService } from './services/todo.service';
import { TodoResolver } from './resolvers/todo.resolver';
import { PostService } from './services/post.service';
import { PostResolver } from './resolvers/post.resolver';
import { AlbumService } from './services/album.service';
import { AlbumResolver } from './resolvers/album.resolver';
import { FilterService } from './services/spinny/filter.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    UsersComponent,
    UserDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatTableModule,
    HttpClientModule
  ],
  providers: [
    UserService,
    UserResolver,
    TodoService,
    TodoResolver,
    PostService,
    PostResolver,
    AlbumService,
    AlbumResolver,
    FilterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
