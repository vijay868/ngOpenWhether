import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output() public sidenavToggle = new EventEmitter();

    constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

    ngOnInit(): void {

    }

    public onToggleSidenav = () => {
        this.sidenavToggle.emit();
    }

    OnUsers(): void {
        this.router.navigate(['./users'], { relativeTo: this.activatedRoute });
    }

    OnDashboard(): void {
        this.router.navigate(['./dashboard'], { relativeTo: this.activatedRoute })
    }
}
