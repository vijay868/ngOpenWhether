import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';


@Component({
    selector: 'app-users',
    templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
    dataSource: User[];
    public displayedColumns = ['name', 'username', 'email', 'phone', 'website', 'company', 'action'];
    constructor(private router: Router, private activatedRoute: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((data: { users: User[] }) => {
            this.dataSource = data.users;
        });
    }

    onViewUser(user: User): void {
        this.router.navigate([`../user/${user.id}`], { relativeTo: this.activatedRoute });
    }
}
