import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { FilterService } from 'src/app/services/spinny/filter.service';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    constructor(private activatedRoute: ActivatedRoute, private filterService: FilterService) {
        this.getFilterData();
    }

    getFilterData(): void {
        this.filterService.getData().subscribe((data: any) => {
            debugger;
        });
    }

    ngOnInit(): void {

    }
}
