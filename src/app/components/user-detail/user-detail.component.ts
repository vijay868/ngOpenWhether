import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Todo } from 'src/app/models/todo.model';
import { Post } from 'src/app/models/post.model';
import { Album } from 'src/app/models/album.model';
import { AlbumResolver } from 'src/app/resolvers/album.resolver';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
    todosDataSource: Todo[];
    public todosDisplayedColumns = ['title', 'completed', 'action'];

    postsDataSource: Post[];
    public postsDisplayedColumns = ['title', 'body', 'action'];

    albumsDataSource: Album[];
    public albumsDisplayedColumns = ['title', 'action'];

    userId: number;
    constructor(private activatedRoute: ActivatedRoute) {
        this.userId = +(activatedRoute.snapshot.paramMap.get('userid'));
    }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((data: { todos: Todo[], posts: Post[], albums: Album[] }) => {
            this.todosDataSource = data.todos;
            this.postsDataSource = data.posts;
            this.albumsDataSource = data.albums;
        });
    }

}
