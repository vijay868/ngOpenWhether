import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Injectable } from '@angular/core';
import { empty } from 'rxjs/internal/observable/empty';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Observable } from 'rxjs';

@Injectable()
export class UserResolver implements Resolve<User[]> {
    constructor(private userService: UserService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): User[] | Observable<User[]> | Promise<User[]> {
        return this.userService.getUsers();
    }

}
