import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Post } from '../models/post.model';
import { Observable } from 'rxjs';
import { PostService } from '../services/post.service';


export class PostResolver implements Resolve<Post[]> {
    constructor(private postService: PostService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Post[] | Observable<Post[]> | Promise<Post[]> {
        return this.postService.getPostsByUserId(+(route.paramMap.get('userid')));
    }

}
