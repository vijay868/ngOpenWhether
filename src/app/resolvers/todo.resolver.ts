import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Todo } from '../models/todo.model';
import { Observable } from 'rxjs';
import { TodoService } from '../services/todo.service';


export class TodoResolver implements Resolve<Todo[]> {
    constructor(private todoService: TodoService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Todo[] | Observable<Todo[]> | Promise<Todo[]> {
        return this.todoService.getTodosByUserId(+(route.paramMap.get('userid')));
    }
}
