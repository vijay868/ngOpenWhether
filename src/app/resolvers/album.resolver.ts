import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Album } from '../models/album.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AlbumService } from '../services/album.service';

@Injectable()
export class AlbumResolver implements Resolve<Album[]> {
    constructor(private albumService: AlbumService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Album[] | Observable<Album[]> | Promise<Album[]> {
        return this.albumService.getAlbumsByUserId(+(route.paramMap.get('userid')));
    }

}
