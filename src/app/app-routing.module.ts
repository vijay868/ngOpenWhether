import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserResolver } from './resolvers/user.resolver';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { TodoResolver } from './resolvers/todo.resolver';
import { PostResolver } from './resolvers/post.resolver';
import { AlbumResolver } from './resolvers/album.resolver';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    resolve: { users: UserResolver }
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'users',
    component: UsersComponent,
    resolve: { users: UserResolver }
  },
  {
    path: 'user/:userid',
    component: UserDetailComponent,
    resolve: {
      todos: TodoResolver,
      posts: PostResolver,
      albums: AlbumResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
