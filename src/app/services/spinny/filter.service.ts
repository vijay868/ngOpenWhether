import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class FilterService {
    constructor(private http: HttpClient) {}

    getData(): Observable<any> {
        return this.http.get(`https://api.spinny.com/api/c/listings/filters/multi_smart/?city=hyderabad&product_type=cars&category=used`);
    }
}
