import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../models/todo.model';
import { Observable } from 'rxjs';


@Injectable()
export class TodoService {
    constructor(private http: HttpClient) {

    }

    getTodosByUserId(userId: number): Observable<Todo[]> {
        return this.http.get<Todo[]>(`https://jsonplaceholder.typicode.com/todos?userid=${userId}`);
    }
}
